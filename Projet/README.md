Repository pour le projet d'IDH 

S'y trouve : 
* le script blastet.py modifié
* Analyse.R pour les analyses réalisées
* exploreGO.R contient les fonctions d'explorations de la Gene Ontology
* un rapport pdf
* des fichiers résultats pour blastet.py avec l'exemple proposé précédemment 

Le script exploreGO est lançable en ligne de commande de tpye Rscript, avec en argument soit `direct`pour les annotations directes, soit `implicit` pour les annotations implicites